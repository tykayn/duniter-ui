# Initial disclaimer
This project is now tracked on our hosted gitlab server at:
https://git.duniter.org/nodes/typescript/modules/duniter-ui

The current github repository is a simple clone taken up to date at each push on the main gitlab repository.

All contributions should be performed on the main gitlab repository.

Pull requests proposed on github would generate more work for the main contributors.

Issues can be submitted on github. However, all issues created on github will be duplicated on gitlab manually and closed with a link to the gitlab issue.

# Original README.md

# Duniter-ui

Duniter graphical interface. This is a developement package which is embedded in [Duniter software](https://git.duniter.org/nodes/typescript/duniter) on build phase.

## Installation

> Requires Nodejs v4+ and Duniter v0.20+

```bash
git clone https://git.duniter.org/nodes/typescript/duniter.git
cd duniter
npm install
```

## Run

```bash
node_modules/brunch/bin/brunch watch --server
```
